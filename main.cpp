#include <iostream>
#include <math.h>
using namespace std;

class complex_cisla 
{
private:
	int valid, imaginary; 

public:
	
	complex_cisla() {};

	complex_cisla(double r) 
	{
		valid = r;
		imaginary = 0;
	}

	complex_cisla(double r, double i) 
	{
		valid = r;
		imaginary = i;
	}

	complex_cisla(const complex_cisla& c) 
	{
		valid = c.valid;
		imaginary = c.imaginary;
	}

	~complex_cisla (){}

		float modyl()
		{
		return sqrt(valid * valid + imaginary * imaginary); 
		}	

	complex_cisla & operator = (complex_cisla & c) 
	{
		valid = c.valid;            
		imaginary = c.imaginary;
		return *this;
	}
	complex_cisla & operator += (complex_cisla & c) 
	{
		valid += c.valid;
		imaginary += c.imaginary;
		return *this;
	}
	complex_cisla operator + (const complex_cisla & c) 
	{
		return complex_cisla(valid + c.valid, imaginary + c.imaginary);
	}
	complex_cisla operator - (const complex_cisla & c) 
	{
		return complex_cisla(valid - c.valid, imaginary - c.imaginary);
	}
	complex_cisla operator * (const complex_cisla & c) 
	{
		return complex_cisla(valid * c.valid - imaginary * c.imaginary);
	}
	complex_cisla operator / (const complex_cisla & c) 
	{
		complex_cisla temp;

		double r = c.valid * c.valid + c.imaginary * c.imaginary;
		temp.valid = (valid * c.valid + imaginary * c.imaginary) / r;
		temp.imaginary = (imaginary * c.valid - valid * c.imaginary) / r;
		return temp;
	}

	friend ostream & operator<< (ostream&, const complex_cisla&);
	friend istream & operator>> (istream&, complex_cisla&);
};
	istream & operator>> (istream & in, complex_cisla & c) 
	{
	cout << "enter the valid part ";
	in >> c.valid;
	cout << "enter the imaginary part ";                            //перегрузка1 >>
	in >> c.imaginary;
	return in;
}


ostream & operator<< (ostream & out, const complex_cisla & c) 
{
	out << "( " << c.valid << ", " << c.imaginary << " )";                      //перегрузка2 <<
	return out;
}

int main() 
{
		complex_cisla c1, c2, c3, c4;
		cin >>c1;
		cin >>c2;
		cin >>c3;         
		cin >>c4;
	cout << endl<<"c1 = " <<c1 << endl;
	cout << "c2 = " <<c2 << endl;
	cout << "c3 = " <<c3 << endl; 
	cout << "c4 = " <<c4 << endl;
	cout << endl << "Modyl c1: " << c1.modyl() << endl; 
	cout << "c1 + c2 = " << (c1 + c2) << endl;
	cout << "c1 - c3 = " << (c1 - c3) << endl;       
	cout << "c1 * c4 = " << (c1 * c4) << endl;
	cout << "c1 / c3 = " << (c1 / c3) << endl;
	return 0;
}
